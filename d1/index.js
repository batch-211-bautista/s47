console.log(document);
console.log(document.querySelector("#txt-first-name"));
// result - element from html
/*
	document - refers to the whole webpage
	querySelector - used to select a specific element (object) as long as it is inside the html tag (HTML ELEMENT);
	- takes a string input that is formatted like CSS selector
	- can select elements regardless if the string is an id, a class, or a tag as long as the element is existing in the webpage
*/

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

/*
	event - actions that the user is doing in our webpage (scroll, click, hover, keypress/type)

	addEventListener - function that lets the webpage to listen to the events perform by the user
		- takes two arguments
			- string - the event to which the HTML element will listen, these are predertimined
			- function - executed by the element once the event (first argument) is triggerred
*/

/*txtFirstName.addEventListener("keyup", (event) => {
	spanFullName.innerHTML = txtFirstName.value;
});*/

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
});

/*
	Make another keyup event where in the span element will record the last name in the forms
	Send the output in the batch google chat
*/

txtLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtLastName.value;
});